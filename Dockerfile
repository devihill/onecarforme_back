FROM openjdk:8-jdk-alpine
VOLUME /tmp
COPY target/one-car-for-me-0.0.1-SNAPSHOT.jar .
ENTRYPOINT ["java","-jar","one-car-for-me-0.0.1-SNAPSHOT.jar"]
