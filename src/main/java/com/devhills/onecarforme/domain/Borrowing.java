package com.devhills.onecarforme.domain;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.devhills.onecarforme.service.dto.BorrowingDTO;

@Entity
@Table(name = "Borrowing")
public class Borrowing {

	public Borrowing(Long id, Car car, User user, Date startDate, Date endDate, String pickUpAddress,
			String returnAddress, String state, String comment, String refusedReason) {
		this.id = id;
		this.car = car;
		this.user = user;
		this.startDate = startDate;
		this.endDate = endDate;
		this.pickupAddress = pickUpAddress;
		this.returnAddress = returnAddress;
		this.state = state;
		this.comment = comment;
		this.refusedReason = refusedReason;
	}

	public Borrowing(Car car, User user, Date startDate, Date endDate, String pickUpAddress, String returnAddress,
			String state, String comment) {
		this.car = car;
		this.user = user;
		this.startDate = startDate;
		this.endDate = endDate;
		this.pickupAddress = pickUpAddress;
		this.returnAddress = returnAddress;
		this.state = state;
		this.comment = comment;
	}

	public Borrowing(BorrowingDTO dto) {
		this.id = dto.getId();
		this.car = dto.getCar();
		this.user = dto.getUser();
		setStartDate(dto.getStartDate());
		setEndDate(dto.getEndDate());
		this.pickupAddress = dto.getPickupAddress();
		this.returnAddress = dto.getReturnAddress();
		this.state = dto.getState();
		this.comment = dto.getComment();
		this.reason = dto.getReason();
		this.refusedReason = dto.getRefusedReason();
		this.valid = dto.isValid();
	}

	public Borrowing() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne
	private Car car;

	@ManyToOne
	private User user;

	@NotNull
	@Column(name = "start_date")
	private Date startDate;

	@Column(name = "end_date")
	private Date endDate;

	@Column(name = "reason")
	private String reason;

	@NotNull
	@Column(name = "pickup_address")
	private String pickupAddress;

	@Column(name = "return_address")
	private String returnAddress;

	@NotNull
	private String state;

//    @Type(type = "text")
	@Column(name = "comment")
	private String comment;

	@Column(name = "refused_reason")
	private String refusedReason;
	
	@Column(name = "is_valid")
	private boolean valid;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Car getCar() {
		return car;
	}

	public void setCar(Car car) {
		this.car = car;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Date getStartDate() {
		return startDate;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public void setStartDate(Date startDate) {
		if (startDate != null && startDate.getTime() > Calendar.getInstance().getTime().getTime()) {
			this.startDate = startDate;
		}
	}

	public void setStartDate(String startDate) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-M-dd HH:mm");
		try {
			this.startDate = sdf.parse(startDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		if (this.startDate != null && endDate.getTime() > this.startDate.getTime()) {
			this.endDate = endDate;
		}
	}

	public void setEndDate(String endDate) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-M-dd HH:mm");
		try {
			this.endDate = sdf.parse(endDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	public String getPickUpAddress() {
		return pickupAddress;
	}

	public void setPickUpAddress(String pickUpAddress) {
		this.pickupAddress = pickUpAddress;
	}

	public String getReturnAddress() {
		return returnAddress;
	}

	public void setReturnAddress(String returnAddress) {
		this.returnAddress = returnAddress;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getPickupAddress() {
		return pickupAddress;
	}

	public void setPickupAddress(String pickupAddress) {
		this.pickupAddress = pickupAddress;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getRefusedReason() {
		return refusedReason;
	}

	public void setRefusedReason(String refusedReason) {
		this.refusedReason = refusedReason;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}
	
	

}
