package com.devhills.onecarforme.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table (name = "Car")
public class Car {
    public Car(){ super();}
    public Car(@NotNull Long id, @NotNull String registrationNumber, byte seat, String mark, String model, String gearBox, String fuel, Long mileage, Date registrationDate) {
    	this.id = id;
        this.registrationNumber = registrationNumber;
        this.seat = seat;
        this.mark = mark;
        this.model = model;
        this.gearBox = gearBox;
        this.fuel = fuel;
        this.mileage = mileage;
        this.registrationDate = registrationDate;
    }


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @NotNull
    @Column(name = "registrationNumber" ,nullable = false)
    private String registrationNumber;

    @NotNull
    @Column(name = "seat" ,nullable = false)
    private byte seat;

    @Column(name = "mark" )
    private String mark;

    @Column(name = "model")
    private String model;

    @Column(name = "gearBox")
    private String gearBox;

    @Column(name = "fuel")
    private String fuel;

    @Column(name = "mileage")
    private Long mileage;

    @Column(name = "registrationDate" ,nullable = false)
    private Date registrationDate;

    public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public byte getSeat() {
        return seat;
    }

    public void setSeat(byte seat) {
        this.seat = seat;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getGearBox() {
        return gearBox;
    }

    public void setGearBox(String gearBox) {
        this.gearBox = gearBox;
    }

    public String getFuel() {
        return fuel;
    }

    public void setFuel(String fuel) {
        this.fuel = fuel;
    }

    public Long getMileage() {
        return mileage;
    }

    public void setMileage(Long mileage) {
        this.mileage = mileage;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }
    
	@Override
	public String toString() {
		return "Car [registrationNumber=" + registrationNumber + ", seat=" + seat + ", mark=" + mark + ", model="
				+ model + ", gearBox=" + gearBox + ", fuel=" + fuel + ", mileage=" + mileage + ", registrationDate="
				+ registrationDate + "]";
	}
    
}

