package com.devhills.onecarforme.domain;

public enum State {
    AVALIDER,
    VALIDE,
    ENCOURS,
    REFUSE,
    TERMINE
}
