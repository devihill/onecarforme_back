package com.devhills.onecarforme.web.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devhills.onecarforme.domain.Borrowing;
import com.devhills.onecarforme.service.BorrowingService;
import com.devhills.onecarforme.service.dto.BorrowingDTO;

@RestController
@RequestMapping("/api")
public class BorrowingResource {

	private final Logger log = LoggerFactory.getLogger(BorrowingResource.class);
	private BorrowingService service;

	public BorrowingResource(BorrowingService service) {
		this.service = service;
	}

	@PostMapping("/borrowing")
	public ResponseEntity<Borrowing> saveBorrowing(@RequestBody BorrowingDTO borrowingDTO) {

		try {
			log.info("L'emprunt " + borrowingDTO.toString() + " est ajouté.");
			Borrowing borrowing = new Borrowing(borrowingDTO);
			return new ResponseEntity<>(service.saveBorrowing(borrowing), HttpStatus.OK);
		} catch (Exception ex) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Throwable ex) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/borrowing/{id}")
	public ResponseEntity<Borrowing> getBorrowing(@PathVariable Long id) {
		try {
			log.info("Récupération de la réservation d'id " + id);
			return new ResponseEntity<>(service.getBorrowing(id), HttpStatus.OK);
		} catch (Exception ex) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@GetMapping("/borrowings")
	public ResponseEntity<List<Borrowing>> getBorrowings() {
		try {
			log.info("Récupération de la liste de toutes les reservations.");
			return new ResponseEntity<>(service.getAllBorrowings(), HttpStatus.OK);
		} catch (Exception ex) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@GetMapping("/user-borrowings/{id}")
	public ResponseEntity<List<Borrowing>> getUserBorrowings(@PathVariable Long id) {
		try {
			log.info("Récupération de la liste de toutes les reservations de l'utilisateur d'id : ");
			return new ResponseEntity<>(service.getAllUserBorrowings(id), HttpStatus.OK);
		} catch (Exception ex) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
}
