package com.devhills.onecarforme.web.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devhills.onecarforme.domain.Car;
import com.devhills.onecarforme.service.CarService;

@RestController
@RequestMapping("/api")
public class CarRessource {

	private final Logger log = LoggerFactory.getLogger(CarRessource.class);

	private final CarService carService;

	public CarRessource(CarService carService) {
		this.carService = carService;
	}

	@PostMapping("/car")
	public ResponseEntity<Car> saveCar(@RequestBody Car car) {
		try {
			log.info("La voiture " + car.toString() + " est ajouté.");
			Car carSaved = this.carService.saveCar(car);

			return new ResponseEntity<>(carSaved, HttpStatus.OK);

		} catch (Exception ex) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/car/{id}")
	public ResponseEntity<Car> getCar(@PathVariable Long id) {
		try {
			log.info("Récupération de la voiture d'id " + String.valueOf(id));
			Car car = this.carService.getCar(id);

			return new ResponseEntity<>(car, HttpStatus.OK);

		} catch (Exception ex) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/car/{id}")
	public ResponseEntity<Car> deleteCar(@PathVariable Long id) {
		try {
			log.info("Suppression de la voiture d'id " + String.valueOf(id));
			this.carService.deleteCar(id);

			return new ResponseEntity<>(HttpStatus.OK);

		} catch (Exception ex) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/cars")
	public ResponseEntity<List<Car>> getCars() {
		try {
			log.info("Récupération de la liste de toutes les voitures.");
			List<Car> cars = this.carService.getAllCars();

			return new ResponseEntity<>(cars, HttpStatus.OK);

		} catch (Exception ex) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/car/quicksearch")
	public ResponseEntity<List<Car>> quickSearch(@RequestParam String quickSearch) {
		try {
			log.info("Recherche rapide : " + quickSearch);
			List<Car> cars = this.carService.quickSearch(quickSearch);

			return new ResponseEntity<>(cars, HttpStatus.OK);

		} catch (Exception ex) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
}
