/**
 * View Models used by Spring MVC REST controllers.
 */
package com.devhills.onecarforme.web.rest.vm;
