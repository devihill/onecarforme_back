package com.devhills.onecarforme.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devhills.onecarforme.domain.Borrowing;

@Repository
public interface BorrowingRepository extends CrudRepository<Borrowing, Long> {
	/*
	 * select * from onecarforme.car left outer join onecarforme.borrowing on
	 * onecarforme.borrowing.car_id = onecarforme.car.id where
	 * onecarforme.borrowing.car_id is null
	 */

	/*
	 * SELECT distinct onecarforme.car.id, registration_number, seat, mark, model,
	 * gear_box, fuel, mileage, registration_date FROM onecarforme.car #,
	 * onecarforme.borrowing.start_date, onecarforme.borrowing.end_date inner join
	 * onecarforme.borrowing on onecarforme.borrowing.car_id = onecarforme.car.id
	 * where onecarforme.borrowing.end_date is not null and
	 * onecarforme.borrowing.state != 4 and onecarforme.borrowing.state != 3 and
	 * (onecarforme.borrowing.start_date > "2019-11-08 19:20:00" or
	 * "2019-11-05 19:20:00" > onecarforme.borrowing.end_date);
	 * 
	 * "2019-11-08 19:20:00" date de fin "2019-11-05 19:20:00" date de début
	 */

	@Query(value = "SELECT b FROM Borrowing b WHERE b.user.id = :id")
	public List<Borrowing> getAllUserBorrowings(@Param("id") Long id);

	@Query(value = "SELECT b FROM Borrowing b WHERE b.startDate <= :dayDate AND b.endDate > :dayDate AND b.state != 'ENCOURS'")
	public List<Borrowing> getAllStartedBorrowings(@Param("dayDate") Date dayDate);

	@Query(value = "SELECT b FROM Borrowing b WHERE b.endDate <= :dayDate AND b.state != 'TERMINE'")
	public List<Borrowing> getAllFinishedBorrowings(@Param("dayDate") Date dayDate);
}
