package com.devhills.onecarforme.repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import com.devhills.onecarforme.domain.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devhills.onecarforme.domain.Car;

@Repository
public interface CarRepository extends CrudRepository<Car, Long> {

    Optional<Car> findByRegistrationNumber(String registrationNumber);
    @Query(value = "SELECT c FROM Car c WHERE "
        + "c.registrationNumber IN :elements "
        + "OR c.seat IN :elements "
        + "OR c.mark IN :elements "
        + "OR c.model IN :elements "
        + "OR c.gearBox IN :elements "
        + "OR c.fuel IN :elements "
        + "OR c.mileage IN :elements "
        + "OR c.registrationDate IN :elements")
    public List<Car> quickSearch(@Param("elements") Collection<String> elements);

}
