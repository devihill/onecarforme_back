package com.devhills.onecarforme.service.dto;

import java.util.Date;

import com.devhills.onecarforme.domain.Borrowing;
import com.devhills.onecarforme.domain.Car;
import com.devhills.onecarforme.domain.User;

public class BorrowingDTO {

	private Long id;

	private Car car;

	private User user;

	private String startDate;
	private String endDate;

	private String pickupAddress;
	private String returnAddress;
	private String state;
	private String comment;
	private String reason;
	private String refusedReason;
	private boolean valid;

	BorrowingDTO(Borrowing borrowing) {
		this.id = borrowing.getId();
		this.car = borrowing.getCar();
		this.user = borrowing.getUser();
		setStartDate(borrowing.getStartDate());
		setEndDate(borrowing.getEndDate());
		this.pickupAddress = borrowing.getPickupAddress();
		this.returnAddress = borrowing.getReturnAddress();
		this.state = borrowing.getState();
		this.comment = borrowing.getComment();
		this.reason = borrowing.getReason();
		this.refusedReason = borrowing.getRefusedReason();
		this.valid = borrowing.isValid();
	}

	public BorrowingDTO() {
		super();
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public void setStartDate(Date startDate) {
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public void setEndDate(Date endDate) {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Car getCar() {
		return car;
	}

	public void setCar(Car car) {
		this.car = car;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getPickUpAddress() {
		return pickupAddress;
	}

	public void setPickUpAddress(String pickUpAddress) {
		this.pickupAddress = pickUpAddress;
	}

	public String getReturnAddress() {
		return returnAddress;
	}

	public void setReturnAddress(String returnAddress) {
		this.returnAddress = returnAddress;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getPickupAddress() {
		return pickupAddress;
	}

	public void setPickupAddress(String pickupAddress) {
		this.pickupAddress = pickupAddress;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getRefusedReason() {
		return refusedReason;
	}

	public void setRefusedReason(String refusedReason) {
		this.refusedReason = refusedReason;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}
	
	

}
