package com.devhills.onecarforme.service;

import java.text.SimpleDateFormat;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.devhills.onecarforme.domain.Borrowing;
import com.devhills.onecarforme.domain.User;
import com.mailjet.client.ClientOptions;
import com.mailjet.client.MailjetClient;
import com.mailjet.client.MailjetRequest;
import com.mailjet.client.MailjetResponse;
import com.mailjet.client.errors.MailjetException;
import com.mailjet.client.errors.MailjetSocketTimeoutException;
import com.mailjet.client.resource.Emailv31;

public class MailjetAPIService {
	public static void sendMail(String content, String subject, String destination_email, String destination_name)
			throws MailjetException, MailjetSocketTimeoutException {
		MailjetClient client;
		MailjetRequest request;
		MailjetResponse response;
		client = new MailjetClient("53cf14f0a1ee77b57a45919618ac6b7d", "38369f6d53f60ce2ef6830c4807c48ce",
				new ClientOptions("v3.1"));
		request = new MailjetRequest(Emailv31.resource).property(Emailv31.MESSAGES,
				new JSONArray().put(new JSONObject()
						.put(Emailv31.Message.FROM,
								new JSONObject().put("Email", "ahmed.chajiddine2017@campus-eni.fr").put("Name",
										"OneCarForMe"))
						.put(Emailv31.Message.TO,
								new JSONArray().put(
										new JSONObject().put("Email", destination_email).put("Name", destination_name)))
						.put(Emailv31.Message.SUBJECT, subject).put(Emailv31.Message.TEXTPART, "My first Mailjet email")
						.put(Emailv31.Message.HTMLPART, content)
						.put(Emailv31.Message.CUSTOMID, "AppGettingStartedTest")));
		response = client.post(request);
		System.out.println(response.getStatus());
		System.out.println(response.getData());
	}

	public static void sendCreateAccountMail(User user) throws MailjetException, MailjetSocketTimeoutException {
		StringBuilder content = new StringBuilder();
		content.append("<p>Madame, Monsieur,</p>\r\n" + "<p>&nbsp;</p>\r\n"
				+ "<p>Un compte <strong><span style=\"font-family: 'Calibri',sans-serif; color: #3366ff;\">OneCarForMe</span></strong> vient de vous &ecirc;tre cr&eacute;e par un administrateur.</p>\r\n"
				+ "<p>&nbsp;</p>\r\n"
				+ "<p>Vous pouvez d&egrave;s &agrave; pr&eacute;sent vous connecter sur la plateforme et profiter de notre solution.</p>\r\n"
				+ "<p>&nbsp;</p>\r\n" + "<p>Lien pour acc&eacute;der &agrave; l'application:&nbsp;</p>\r\n"
				+ "<p><a href=\\\"http://5.196.89.184/#/\\\">http://5.196.89.184/</a></p>\r\n" + "<p>&nbsp;</p>\r\n"
				+ "<p>Login: ").append(user.getLogin()).append("&nbsp;</p>\r\n" + "<p>Mot de passe: ")
				.append("XsqdkJh45gf!").append("</p>\r\n" + "<p>&nbsp;</p>\r\n" + "<p>Bien &agrave; vous,</p>\r\n"
						+ "<p>&nbsp;</p>\r\n" + "<p>L'&eacute;quipe OneCarForMe.</p>");
		String subject = "Un compte OneCarForMe vous a été crée !";
		MailjetAPIService.sendMail(content.toString(), subject, user.getEmail(), "");
	}

	public static void sendValideMail(Borrowing borrowing) throws MailjetException, MailjetSocketTimeoutException {
		StringBuilder content = new StringBuilder();
		content.append("<p>&nbsp;</p>\r\n"
				+ "<p>Votre demande d'emprunt a été validée dans&nbsp;<strong><span style=\"font-family: 'Calibri',sans-serif; color: #3366ff;\">OneCarForMe :</span></strong></p>\r\n"
				+ "<ul>\r\n" + "<li><strong><span style=\"font-family: 'Calibri',sans-serif; color: #3366ff;\">")
				.append(borrowing.getUser().getLastName()).append(" ").append(borrowing.getUser().getFirstName())
				.append("</span></strong></li>\r\n"
						+ "<li><strong><span style=\"font-family: 'Calibri',sans-serif; color: #3366ff;\">")
				.append("Date de début : ")
				.append(new SimpleDateFormat("dd/MM/yyyy HH:mm").format(borrowing.getStartDate()))
				.append("</span></strong></li>\r\n"
						+ "<li><strong><span style=\"font-family: 'Calibri',sans-serif; color: #3366ff;\">")
				.append("Date de fin : ")
				.append(new SimpleDateFormat("dd/MM/yyyy HH:mm").format(borrowing.getEndDate()))
				.append("</span></strong></li>\r\n"
						+ "<li><span style=\"color: #3366ff; font-family: Calibri, sans-serif;\"><strong>")
				.append("Adresse de récupération : ").append(borrowing.getPickupAddress())
				.append("</strong></span></li>\r\n"
						+ "<li><span style=\"color: #3366ff; font-family: Calibri, sans-serif;\"><strong>")
				.append("Adresse de retour : ").append(borrowing.getReturnAddress())
				.append("</strong></span></li>\r\n" + "</ul>\r\n" + "<p>&nbsp;</p>\r\n"
						+ "<p>Vous pouvez d&egrave;s &agrave; pr&eacute;sent vous connecter pour consulter la demande.</p>\r\n"
						+ "<p>&nbsp;</p>\r\n" + "<p>Lien pour acc&eacute;der &agrave; l'application:&nbsp;</p>\r\n"
						+ "<p><a href=\\\"http://5.196.89.184/#/\\\">http://5.196.89.184/</a></p>\r\n"
						+ "<p>&nbsp;</p>\r\n" + "<p>Bien &agrave; vous,</p>\r\n" + "<p>&nbsp;</p>\r\n"
						+ "<p>L'&eacute;quipe OneCarForMe.</p>");

		String subject = "Demande d'emprunt validée";
		MailjetAPIService.sendMail(content.toString(), subject, borrowing.getUser().getEmail(), "");

	}

	public static void sendRefuseMail(Borrowing borrowing) throws MailjetException, MailjetSocketTimeoutException {
		StringBuilder content = new StringBuilder();
		content.append("<p>&nbsp;</p>\r\n"
				+ "<p>Votre demande d'emprunt a été refusée dans&nbsp;<strong><span style=\"font-family: 'Calibri',sans-serif; color: #3366ff;\">OneCarForMe :</span></strong></p>\r\n"
				+ "<ul>\r\n" + "<li><strong><span style=\"font-family: 'Calibri',sans-serif; color: #3366ff;\">")
				.append(borrowing.getUser().getLastName()).append(" ").append(borrowing.getUser().getFirstName())
				.append("</span></strong></li>\r\n"
						+ "<li><strong><span style=\"font-family: 'Calibri',sans-serif; color: #3366ff;\">")
				.append("Date de début : ")
				.append(new SimpleDateFormat("dd/MM/yyyy HH:mm").format(borrowing.getStartDate()))
				.append("</span></strong></li>\r\n"
						+ "<li><strong><span style=\"font-family: 'Calibri',sans-serif; color: #3366ff;\">")
				.append("Date de fin : ")
				.append(new SimpleDateFormat("dd/MM/yyyy HH:mm").format(borrowing.getEndDate()))
				.append("</span></strong></li>\r\n"
						+ "<li><span style=\"color: #3366ff; font-family: Calibri, sans-serif;\"><strong>")
				.append("Adresse de récupération : ").append(borrowing.getPickupAddress())
				.append("</strong></span></li>\r\n"
						+ "<li><span style=\"color: #3366ff; font-family: Calibri, sans-serif;\"><strong>")
				.append("Adresse de retour : ").append(borrowing.getReturnAddress())
				.append("</strong></span></li>\r\n" + "</ul>\r\n" + "<p>Motif : <strong>")
				.append(borrowing.getRefusedReason())
				.append("</strong></p>\r\n"
						+ "<p>Vous pouvez d&egrave;s &agrave; pr&eacute;sent vous connecter pour consulter la demande.</p>\r\n"
						+ "<p>&nbsp;</p>\r\n" + "<p>Lien pour acc&eacute;der &agrave; l'application:&nbsp;</p>\r\n"
						+ "<p><a href=\\\"http://5.196.89.184/#/\\\">http://5.196.89.184/</a></p>\r\n"
						+ "<p>&nbsp;</p>\r\n" + "<p>Bien &agrave; vous,</p>\r\n" + "<p>&nbsp;</p>\r\n"
						+ "<p>L'&eacute;quipe OneCarForMe.</p>");

		String subject = "Demande d'emprunt refusée";
		MailjetAPIService.sendMail(content.toString(), subject, borrowing.getUser().getEmail(), "");
	}

	public static void sendAvalideMail(Borrowing borrowing, List<User> adminList)
			throws MailjetException, MailjetSocketTimeoutException {

		StringBuilder content = new StringBuilder();
		content.append("<p>&nbsp;</p>\r\n"
				+ "<p>Une nouvelle demande d'emprunt est disponible dans&nbsp;<strong><span style=\"font-family: 'Calibri',sans-serif; color: #3366ff;\">OneCarForMe :</span></strong></p>\r\n"
				+ "<ul>\r\n" + "<li><strong><span style=\"font-family: 'Calibri',sans-serif; color: #3366ff;\">")
				.append(borrowing.getUser().getLastName()).append(" ").append(borrowing.getUser().getFirstName())
				.append("</span></strong></li>\r\n"
						+ "<li><strong><span style=\"font-family: 'Calibri',sans-serif; color: #3366ff;\">")
				.append("Date de début : ")
				.append(new SimpleDateFormat("dd/MM/yyyy HH:mm").format(borrowing.getStartDate()))
				.append("</span></strong></li>\r\n"
						+ "<li><strong><span style=\"font-family: 'Calibri',sans-serif; color: #3366ff;\">")
				.append("Date de fin : ")
				.append(new SimpleDateFormat("dd/MM/yyyy HH:mm").format(borrowing.getEndDate()))
				.append("</span></strong></li>\r\n"
						+ "<li><span style=\"color: #3366ff; font-family: Calibri, sans-serif;\"><strong>")
				.append("Adresse de récupération : ").append(borrowing.getPickupAddress())
				.append("</strong></span></li>\r\n"
						+ "<li><span style=\"color: #3366ff; font-family: Calibri, sans-serif;\"><strong>")
				.append("Adresse de retour : ").append(borrowing.getReturnAddress())
				.append("</strong></span></li>\r\n" + "</ul>\r\n" + "<p>&nbsp;</p>\r\n"
						+ "<p>Vous pouvez d&egrave;s &agrave; pr&eacute;sent vous connecter sur la plateforme pour y r&eacute;pondre.</p>\r\n"
						+ "<p>&nbsp;</p>\r\n" + "<p>Lien pour acc&eacute;der &agrave; l'application:&nbsp;</p>\r\n"
						+ "<p><a href=\\\"http://5.196.89.184/#/\\\">http://5.196.89.184/</a></p>\r\n"
						+ "<p>&nbsp;</p>\r\n" + "<p>Bien &agrave; vous,</p>\r\n" + "<p>&nbsp;</p>\r\n"
						+ "<p>L'&eacute;quipe OneCarForMe.</p>");

		String subject = "Nouvelle demande d'emprunt";
		for (User admin : adminList) {
			MailjetAPIService.sendMail(content.toString(), subject, admin.getEmail(), "");
		}

	}
}
