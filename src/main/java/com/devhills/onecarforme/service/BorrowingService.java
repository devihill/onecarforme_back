package com.devhills.onecarforme.service;

import java.util.List;

import com.devhills.onecarforme.domain.Borrowing;

public interface BorrowingService {

	/***
	 * Sauvegarde une reservation dans la base
	 * 
	 * @param borrowing : reservation à sauvegarder
	 * @return Retourne la reservation ajoutée avec l'id si elle a été créée
	 */
	public Borrowing saveBorrowing(Borrowing borrowing) throws Throwable;

	/***
	 * Retourne une liste de toutes les réservations
	 * 
	 * @return Retourne la liste de toutes les réservation
	 */
	public List<Borrowing> getAllBorrowings();

	/***
	 * Retourne une liste de toutes les réservations d'un utilisateur
	 * 
	 * @return Retourne la liste de toutes les réservation
	 */
	public List<Borrowing> getAllUserBorrowings(Long id);

	/***
	 * renvoie un emprut en fonction de son id
	 * 
	 * @param id l'id de l'emprunt
	 * @return Borrowing borrowing
	 */
	public Borrowing getBorrowing(Long id);

	/***
	 * met à jour l'état de la réservation
	 */
	public void updateState();
}
