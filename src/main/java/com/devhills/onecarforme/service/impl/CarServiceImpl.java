package com.devhills.onecarforme.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.devhills.onecarforme.domain.Car;
import com.devhills.onecarforme.repository.CarRepository;
import com.devhills.onecarforme.service.CarService;

@Service
@Transactional
public class CarServiceImpl implements CarService {
	private CarRepository carRepo;

	public CarServiceImpl(CarRepository carRepo) {
		super();
		this.carRepo = carRepo;
	}

	@Override
	public Car saveCar(Car car) {
		if (car.getRegistrationDate() == null) {
			Calendar calendar = Calendar.getInstance();
			car.setRegistrationDate(calendar.getTime());
		}
		return carRepo.save(car);
	}

	@Override
	public List<Car> getAllCars() {
		Iterable<Car> itCars = carRepo.findAll();
		List<Car> cars = new ArrayList<Car>();
		itCars.forEach(cars::add);
		return cars;
	}

	@Override
	public List<Car> quickSearch(String quickSearch) {
		List<String> elements = new ArrayList<String>(Arrays.asList(quickSearch.split(" ")));
		return this.carRepo.quickSearch(elements);
	}

	@Override
	public Car getCar(Long id) {
		Optional<Car> car = this.carRepo.findById(id);
		if (car.isPresent()) {
			return car.get();
		} else {
			return null;
		}
	}

	@Override
	public void deleteCar(Long id) {
		this.carRepo.deleteById(id);
	}
}
