package com.devhills.onecarforme.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.devhills.onecarforme.domain.Authority;
import com.devhills.onecarforme.domain.Borrowing;
import com.devhills.onecarforme.domain.Car;
import com.devhills.onecarforme.domain.User;
import com.devhills.onecarforme.repository.BorrowingRepository;
import com.devhills.onecarforme.repository.CarRepository;
import com.devhills.onecarforme.repository.UserRepository;
import com.devhills.onecarforme.service.BorrowingService;
import com.devhills.onecarforme.service.MailjetAPIService;
import com.devhills.onecarforme.service.UserService;

@Service
@Transactional
public class BorrowingServiceImpl implements BorrowingService {

	private BorrowingRepository repository;
	private UserRepository userRepo;
	private CarRepository carRepo;
	private UserService userService;
	String messageEx;

	public BorrowingServiceImpl(BorrowingRepository repository, UserRepository userRepo, CarRepository carRepo,
			UserService userService) {
		super();
		this.repository = repository;
		this.userRepo = userRepo;
		this.carRepo = carRepo;
		this.userService = userService;
	}

	@Override
	public Borrowing saveBorrowing(Borrowing borrowing) throws Throwable {
		if (verifBorrowing(borrowing)) {
			Borrowing savedBorrowing = repository.save(borrowing);

			if (savedBorrowing.getState().equals("AVALIDER")) {
				Authority admin = new Authority();
				admin.setName("ROLE_ADMIN");
				MailjetAPIService.sendAvalideMail(savedBorrowing, this.userRepo.getAllUserFromAuthoritie(admin));
			} else if (savedBorrowing.getState().equals("VALIDE")) {
				MailjetAPIService.sendValideMail(savedBorrowing);
			} else if (savedBorrowing.getState().equals("REFUSE")) {
				MailjetAPIService.sendRefuseMail(savedBorrowing);
			}
			
			return savedBorrowing;
		} else
			throw new Exception(messageEx);
	}

	@Override
	public Borrowing getBorrowing(Long id) {
		Optional<Borrowing> borrowing = repository.findById(id);
		if (borrowing.isPresent()) {
			return borrowing.get();
		} else {
			return null;
		}
	}

	@Override
	public List<Borrowing> getAllBorrowings() {
		Iterable<Borrowing> itBorrowings = repository.findAll();
		List<Borrowing> borrowings = new ArrayList<Borrowing>();
		itBorrowings.forEach(borrowings::add);
		return borrowings;
	}

	@Override
	@Scheduled(cron = "0 * * ? * *") // Lancement toutes les minutes
	public void updateState() {
		// Changement des emprunts au statut en cours
		List<Borrowing> startedBorrowings = repository.getAllStartedBorrowings(new Date());
		if (startedBorrowings.size() > 0) {
			for (Borrowing startedBorrowing : startedBorrowings) {
				if (startedBorrowing.getState().equals("VALIDE")) {
					startedBorrowing.setState("ENCOURS");
				} else if (startedBorrowing.getState().equals("REFUSE")) {
					startedBorrowing.setState("TERMINE");
				} else if (startedBorrowing.getState().equals("AVALIDER")) {
					startedBorrowing.setValid(false);
					startedBorrowing.setRefusedReason("Pas d'intervention sur l'emprunt.");
					startedBorrowing.setState("TERMINE");
				}
			}
			repository.saveAll(startedBorrowings);
		}

		// Changement des emprunts au statut en terminé
		List<Borrowing> finishedBorrowings = repository.getAllFinishedBorrowings(new Date());
		if (finishedBorrowings.size() > 0) {
			for (Borrowing finishedBorrowing : finishedBorrowings) {
				if (finishedBorrowing.getState().equals("ENCOURS")) {
					finishedBorrowing.setState("TERMINE");
				}
			}
			repository.saveAll(startedBorrowings);
		}
	}

	/***
	 * verifie que l' empreinte est correct les dates sont cohérentes le user et la
	 * voiture existent l'état est cohérent l'oiseau est sorti de sa cage
	 * 
	 * @param borrowing
	 * @return
	 */
	public Boolean verifBorrowing(Borrowing borrowing) {
		User user = borrowing.getUser();
		Car car = borrowing.getCar();

		if (borrowing.getEndDate().getTime() < borrowing.getStartDate().getTime()
				&& borrowing.getStartDate().getTime() < Calendar.getInstance().getTime().getTime()) {
			messageEx = "la date n'est pas correcte";
			return false;
		}

		if (car == null && user == null && !userRepo.findById(user.getId()).isPresent()
				&& !carRepo.findById(car.getId()).isPresent()) {
			messageEx = "l'utilisateur ou la voiture n'est pas correcte";
			return false;
		}
		return true;
	}

	@Override
	public List<Borrowing> getAllUserBorrowings(Long id) {
		return repository.getAllUserBorrowings(id);
	}
}
