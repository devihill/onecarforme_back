package com.devhills.onecarforme.service;

import java.util.List;

import com.devhills.onecarforme.domain.Car;

public interface CarService {
	/***
	 * Sauvegarde une voiture dans la base
	 * 
	 * @param car : voiture àa sauvegarder
	 * @return Retourne la voiture ajoutée avec l'id si elle a été créée
	 */
	public Car saveCar(Car car);

	/**
	 * Récupère une voiture dans la base
	 * 
	 * @param id Id de la voiture que l'on souhaite récupérer
	 * @return Retourne la voiture recherchée
	 */
	public Car getCar(Long id);

	/**
	 * Méthode de suppression d'une voiture dans la base
	 * 
	 * @param id Id de la voiture que l'on souhaite supprimer
	 */
	public void deleteCar(Long id);

	/***
	 * Retourne une liste de toutes les voitures
	 * 
	 * @return Retourne la liste de toutes les voitures
	 */
	public List<Car> getAllCars();

	/**
	 * Retourne une liste de voitures en fonction de la recherche rapide reçue
	 * 
	 * @param quickSearch : Recherche à réaliser
	 * @return Retourne la liste des voitures trouvées
	 */
	public List<Car> quickSearch(String quickSearch);
}
